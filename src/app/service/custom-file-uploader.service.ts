import {Injectable} from '@angular/core';
import {HttpClient, HttpEvent, HttpRequest} from "@angular/common/http";
import {Observable} from "rxjs";
import {AppConfig} from "./app.config";

@Injectable({
  providedIn: 'root'
})
export class FileUploadService {

  private uploadUrl = `${AppConfig.apiUrl}api/file/v1/upload`;

  constructor(private http: HttpClient) {
  }

  upload(formData : FormData): Observable<HttpEvent<any>> {
    const req = new HttpRequest('POST', this.uploadUrl, formData, {
      reportProgress: true,
    });
    return this.http.request(req);
  }
}
