import {Component, ViewChild} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Project} from "../model/project.model";
import {NgForm} from "@angular/forms";
import {AuditModel} from "../model/audit.model";
import {AppConfig} from "../service/app.config";

@Component({
  selector: 'app-file-uploader',
  templateUrl: './file-uploader.component.html',
  styleUrls: ['./file-uploader.component.css'],
})
export class FileUploaderComponent {

  @ViewChild('fileUploadForm') form: NgForm | undefined;
  private selectedFile: File | null = null;
  errorList: string[] = [];
  project: string = '';
  audit: any = '';
  projects: Project[] = [];
  private proId: any;
  auditPlans: AuditModel[] = [];
  isCerfied = false;
  disableButton = false;
  progress: number = 0;
  stock: any = {};
  disableProgressBar : boolean = true;

  constructor(private http: HttpClient) {
  }

  onFileSelected(event: any) {
    this.selectedFile = event.target.files[0];
  }

  uploading_status : string = "";

  submitForm() {
    this.disableProgressBar = false;
    // this.isCerfied = true;
    this.disableButton = true;
    const formData = new FormData();
    if (this.selectedFile && this.project != '' && this.audit != '') {
      console.log(("uploading file "))
      formData.append('file', this.selectedFile);
      formData.append('project_id', this.projectIdSelected.toString());
      formData.append('project_name', this.projectName);
      formData.append('audit', this.audit);
      formData.append('proId', this.proId);

      this.errorList = [];

      this.http.post<any>(`${AppConfig.apiUrl}api/file/v1/upload`, formData, {
        observe: 'events',
        reportProgress: true
      }).subscribe(
        event => {
          console.log("event", event);
          console.log("event", event.type);
          if (event.type === 0){
            this.uploading_status = "Uploading";
            console.log(0)
            this.progress = 25;
          }else if (event.type === 1) {
            // @ts-ignore
            this.progress = Math.round((100 * (event.loaded) / event.total))*2/4;
            this.uploading_status = "Start reading";
          } else if (event.type === 4) {

            this.uploading_status = "Successfully Completed";
            this.progress = 100;
            this.disableButton = false;
          }
        },
        error => {
          this.uploading_status = "Completed with Errors";
          this.progress = 100;
          this.disableButton = false;
          console.error('An error occurred during file reading:', error);
          let err;
          error.error.forEach((e: any) => {
            err = e.location + " : " + e.error + " : " + ((e.errorValue == null) ? " " : "Error : " + e.errorValue)
              + ((e.correctValue == null) ? " " : " Must be correct as : " + e.correctValue);
            this.errorList.push(err);
            // alert("error")
            // @ts-ignore
            this.form.resetForm()
          })
          // this.fileReadProgress = undefined;
        },
        () => {
          this.disableButton = false;
        });
    }
  }


  searchProjects() {
    // console.log(this.project)
    this.http.get<Project[]>(`${AppConfig.apiUrl}api/project/v1/search?name=${this.project}`)
      .subscribe(projects => {
        this.projects = projects
        // console.log(this.projects)
      });
  }

  projectIdSelected: string = "";
  projectName: string = "";

  onProjectOptionSelected() {
    const project = this.projects.find(p => p.proName === this.project);
    if (project) {
      this.projectIdSelected = project.proCode;
      this.projectName = project.proName;
      this.proId = project.id;
      // console.log(this.projectIdSelected)
      this.getAudits(this.proId);
    }
  }

  clearErrors() {
    this.errorList = [];
    // @ts-ignore
    this.form.resetForm()
  }

  getAudits(proid: number) {
    console.log("get audits plans for : " + proid)
    this.http.get<any>(`${AppConfig.apiUrl}api/audit/v1/getAuditPlansByProId?proId=${this.proId}`)
      .subscribe(audits => {
        this.auditPlans = audits
        console.log(audits)
      });
  }

  onAuditOptionSelected() {
    // const pro = this.auditPlans.find(a => a.planId === this.audit)
    let auditObject = this.auditPlans.find(a => a.planId == this.audit);
    // @ts-ignore
    this.isCerfied = auditObject.certified;
    console.log("selected audit ", this.audit)
    console.log("selected audit ", auditObject)
  }
}
